import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { AngularFireAuth } from 'angularfire2/auth';
import { User } from '../commons/user';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { PerfilPage } from '../pages/perfil/perfil';
import { RecomendacionesPage } from '../pages/recomendaciones/recomendaciones';
import { Carrera } from '../commons/carrera';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;
  user: User;
  usuarios: Observable<User[]>;
  usersRef: AngularFirestoreCollection<User[]>;
  pages: Array<{title: string, component: any, user?: any }>;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    private afAuth: AngularFireAuth,
    private database: AngularFirestore
  ) {
    this.initializeApp();
    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Carreras', component: HomePage },
    ];
    this.usersRef = this.database.collection<User[]>("users");

    this.usuarios = this.usersRef.snapshotChanges().map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data() as User;
        const id = a.payload.doc.id;
        return { id, ...data }
      })
    })
    this.afAuth.authState.subscribe(sesion => {
      if(sesion){
        this.usuarios.forEach(db_usuarios => {
          db_usuarios.forEach(usuario => {
            if(sesion.email == usuario.email){
              this.user = usuario;
              this.pages = [
                { title: 'Carreras', component: HomePage, user: this.user },
                { title: 'Perfil', component: PerfilPage, user: this.user },
              ];
            }
          })
        })
      }
    });

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component, {
      id: page.user
    });
  }

  verifyUser(){
    if(this.afAuth.authState){
      this.afAuth.authState.subscribe(user => {
        
      })
    }
  }
}
