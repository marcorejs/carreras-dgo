import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { IonicImageViewerModule } from 'ionic-img-viewer';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { VerCarreraPage } from '../pages/home/ver-carrera';
import { VerRutaPage } from '../pages/home/ver-ruta';
import { VerVideoPage } from '../pages/home/video';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';
import { InscripcionPage } from '../pages/inscripcion/inscripcion';
import { PayPal } from '@ionic-native/paypal';
import { PromocionesPage } from '../pages/promociones/promociones';
import { PreparacionFisicaPage } from '../pages/preparacion-fisica/preparacion-fisica';
import { PatrocinadoresPage } from '../pages/patrocinadores/patrocinadores';
import { PremiosPage } from '../pages/premios/premios';
import { PreRegistroPage } from '../pages/pre-registro/pre-registro';
import { ComunidadPage } from '../pages/comunidad/comunidad';
import { MemesPage } from '../pages/memes/memes';
import { GaleriaPage } from '../pages/galeria/galeria';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { EncuestaPage } from '../pages/encuesta/encuesta';
import { RealidadPage } from '../pages/realidad/realidad';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import {LoginPage} from "../pages/login/login";
import {RegisterPage} from "../pages/register/register";
import {AngularFireAuth} from "angularfire2/auth";
import { PerfilPage } from '../pages/perfil/perfil';
import { UserConfigPage } from '../pages/user-config/user-config';
import { RecomendacionesPage } from '../pages/recomendaciones/recomendaciones';
import { AiProvider } from '../providers/ai/ai';

export const FIREBASE_CONFIG = {
  apiKey: "AIzaSyCQk8dT3Px1moCZ6tDfGDC1zsNTO1q3dVk",
  authDomain: "administrador-espacios.firebaseapp.com",
  databaseURL: "https://administrador-espacios.firebaseio.com",
  projectId: "administrador-espacios",
  storageBucket: "administrador-espacios.appspot.com",
  messagingSenderId: "250008479103"
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    VerCarreraPage,
    VerRutaPage,
    VerVideoPage,
    InscripcionPage,
    PromocionesPage,
    PreparacionFisicaPage,
    PatrocinadoresPage,
    PremiosPage,
    PreRegistroPage,
    ComunidadPage,
    MemesPage,
    GaleriaPage,
    EncuestaPage,
    RealidadPage,
    LoginPage,
    RegisterPage,
    PerfilPage,
    UserConfigPage,
    RecomendacionesPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(FIREBASE_CONFIG),
    AngularFirestoreModule,
    IonicImageViewerModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    VerCarreraPage,
    VerRutaPage,
    VerVideoPage,
    InscripcionPage,
    PromocionesPage,
    PreparacionFisicaPage,
    PatrocinadoresPage,
    PremiosPage,
    PreRegistroPage,
    ComunidadPage,
    MemesPage,
    GaleriaPage,
    EncuestaPage,
    RealidadPage,
    LoginPage,
    RegisterPage,
    PerfilPage,
    UserConfigPage,
    RecomendacionesPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    YoutubeVideoPlayer,
    PayPal,
    PhotoViewer,
    InAppBrowser,
    AngularFireAuth,
    AiProvider
  ]
})
export class AppModule { }
