import { User } from "./user";


export interface Carrera {
    nombre: string,
    ruta: Array<any>,
    servicios: Array<any>,
    costo: string,
    anunciantes: Array<any>,
    distancia: string,
    estacionamientos?: Array<any>,
    kit?: any,
    memes?: Array<any>,
    galeria?: Array<any>,
    seguidores?: Array<User>,
    id?: string,
    disciplina: string
}