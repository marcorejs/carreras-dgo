import { Carrera } from "./carrera";

export interface User {
    nombre: string,
    email: string,
    vmn: number,
    deporte_fav?: string,
    distancia_pref?: string,
    prom_costo?: number,
    eventos_seguidos?: Array<Carrera>,
    id?: string,
    genero?: string
}