import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Carrera } from '../../commons/carrera';
import { GaleriaPage } from '../galeria/galeria';
import { MemesPage } from '../memes/memes';

@IonicPage()
@Component({
  selector: 'page-comunidad',
  templateUrl: 'comunidad.html',
})
export class ComunidadPage {

  carrera: Carrera;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams
  ) {
    this.carrera = this.navParams.get('id');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ComunidadPage');
  }

  galeriaPage(){
    this.navCtrl.push(GaleriaPage, {
      id: this.carrera
    });
  }

  memesPage(){
    this.navCtrl.push(MemesPage, {
      id: this.carrera
    });
  }

}
