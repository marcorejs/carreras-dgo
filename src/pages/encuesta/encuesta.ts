import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-encuesta',
  templateUrl: 'encuesta.html',
})
export class EncuestaPage {

  opiniones: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams
  ) {
    this.opiniones = ['Excelente', 'Buen@', 'Regular', 'Mal@', 'Pesim@']
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EncuestaPage');
  }

}
