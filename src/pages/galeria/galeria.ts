import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Carrera } from '../../commons/carrera';
import { PhotoViewer } from '@ionic-native/photo-viewer';

@IonicPage()
@Component({
  selector: 'page-galeria',
  templateUrl: 'galeria.html',
})
export class GaleriaPage {

  carrera: Carrera;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private pv: PhotoViewer
  ) {
    this.carrera = this.navParams.get('id');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GaleriaPage');
  }

  viewFoto(img: string){
    this.pv.show(img);
  }

}
