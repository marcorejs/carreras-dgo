import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { Carrera } from '../../commons/carrera';
import { VerCarreraPage } from './ver-carrera';
import { LoginPage } from "../login/login";
import { AngularFireAuth } from 'angularfire2/auth';
import { PerfilPage } from '../perfil/perfil';
import { User } from '../../commons/user';
import { RecomendacionesPage } from '../recomendaciones/recomendaciones';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  usuarios: Observable<User[]>;
  usersRef: AngularFirestoreCollection<User[]>;
  carreras: Observable<Carrera[]>;
  userExist: any;
  carrerasRef: AngularFirestoreCollection<Carrera[]>;
  constructor(
    public navCtrl: NavController,
    public database: AngularFirestore,
    private afAuth: AngularFireAuth
  ) {
    this.carrerasRef = this.database.collection<Carrera[]>("carreras");
    this.usersRef = this.database.collection<User[]>("users");

    this.usuarios = this.usersRef.snapshotChanges().map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data() as User;
        const id = a.payload.doc.id;
        return { id, ...data }
      })
    });

    this.carreras = this.carrerasRef.snapshotChanges().map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data() as Carrera;
        const id = a.payload.doc.id;
        return { id, ...data }
      })
    });
  }

  verCarrera(_carrera: Carrera) {
    this.afAuth.auth.onAuthStateChanged(sesion => {
      if (sesion) {
        this.usuarios.forEach(db_usuarios => {
          db_usuarios.forEach(usuario => {
            if (sesion.email == usuario.email) {
              console.log(usuario.email);
              this.navCtrl.setRoot(VerCarreraPage, {
                id: _carrera,
                u: usuario
              });
            }
          })
        })
      } else {
        this.navCtrl.setRoot(VerCarreraPage, {
          id: _carrera,
          u: 0
        });
      }
    })

  }

  goToIDK(): void {
    this.afAuth.auth.onAuthStateChanged(sesion => {
      if (sesion) {
        this.usuarios.forEach(db_usuarios => {
          db_usuarios.forEach(usuario => {
            if (sesion.email == usuario.email) {
              this.navCtrl.setRoot(PerfilPage, {
                id: usuario
              });
            }
          })
        })

      } else {
        this.navCtrl.setRoot(LoginPage);
      }
    })

  }

  recomendacionesPage() {
    this.afAuth.auth.onAuthStateChanged(sesion => {
      if (sesion) {
        this.usuarios.forEach(db_usuarios => {
          db_usuarios.forEach(usuario => {
            if (sesion.email == usuario.email) {
              console.log("hola");
              let aux = this.database.collection<Carrera[]>("carreras").snapshotChanges().map(actions => {
                return actions.map(a => {
                  const data = a.payload.doc.data() as Carrera;
                  const id = a.payload.doc.id;
                  return { id, ...data }
                })
              });
              aux.forEach(db_carreras => {
                this.navCtrl.push(RecomendacionesPage, {
                  id: db_carreras,
                  u: usuario
                });
              })
            }
          })
        })
      } else {
        console.log("no se arma");
      }
    })
  }

  irRecomendaciones(_user: User) {
    this.carreras.forEach(db_carreras => {
      this.navCtrl.setRoot(RecomendacionesPage, {
        id: db_carreras,
        u: _user
      });
    })
  }


}
