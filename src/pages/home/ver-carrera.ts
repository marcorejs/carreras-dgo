import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams} from 'ionic-angular';
import { AngularFirestore } from 'angularfire2/firestore';
import { Carrera } from '../../commons/carrera';
import { VerRutaPage } from './ver-ruta';
import { InscripcionPage } from '../inscripcion/inscripcion';
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal';
import { PatrocinadoresPage } from '../patrocinadores/patrocinadores';
import { PremiosPage } from '../premios/premios';
import { PreparacionFisicaPage } from '../preparacion-fisica/preparacion-fisica';
import { PromocionesPage } from '../promociones/promociones';
import { PreRegistroPage } from '../pre-registro/pre-registro';
import { ComunidadPage } from '../comunidad/comunidad';
import { EncuestaPage } from '../encuesta/encuesta';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { User } from '../../commons/user';

@Component({
    selector: 'page-home',
    templateUrl: 'ver-carrera.html'
})
export class VerCarreraPage {

    @ViewChild('map') mapElement: ElementRef;
    map: any;
    carrera: Carrera;
    user: User;
    carrera_seguidores: number;
    following: any;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public database: AngularFirestore,
        private payPal: PayPal,
        private browser: InAppBrowser,
        private alertCtrl: AlertController
    ) {
        this.carrera = this.navParams.get('id');
        this.user = this.navParams.get('u');
        console.log(this.carrera);
        this.carrera_seguidores = (this.carrera.seguidores != undefined) ? this.carrera.seguidores.length:0;
        if(this.user.eventos_seguidos){
            if (this.contains(this.user.eventos_seguidos, this.carrera.nombre)) {
                this.following = true;
            } else {
                this.following = false;
            }
        }
    }
    verRuta() {
        this.navCtrl.push(VerRutaPage, {
            id: this.carrera
        })
    }

    inscripcionPage(){
        this.navCtrl.push(InscripcionPage, {
            id: this.carrera
        })
    }

    pay() {
        console.log("LLamar paypal");
        this.payPal.init({
            PayPalEnvironmentProduction: 'AUmu_v627GtdTKLpxklC1Q-v3y0P3HN2EvQRj4e_ZcrTGCDk9XODhjEFf-9J_XkKaTjMsYFPm5jj9U5A',
            PayPalEnvironmentSandbox: 'AYnXw7j0ORCEKDAJlqCFMbMociws-Wb6kkFqIrCALZL7UuVK2oMpNe4DYOX9-gjvmrk8ShVwUfVDVuDA'
        }).then(() => {
            console.log("paypal init");
            // Environments: PayPalEnvironmentNoNetwork, PayPalEnvironmentSandbox, PayPalEnvironmentProduction
            this.payPal.prepareToRender('PayPalEnvironmentSandbox', new PayPalConfiguration({
                // Only needed if you get an "Internal Service Error" after PayPal login!
                //payPalShippingAddressOption: 2 // PayPalShippingAddressOptionPayPal
            })).then(() => {
                console.log("render");
                let payment = new PayPalPayment('150.00', 'MXN', 'Pago de carrera: ' + this.carrera.nombre, 'sale');
                this.payPal.renderSinglePaymentUI(payment).then(() => {
                    // Successfully paid

                    // Example sandbox response
                    //
                    // {
                    //   "client": {
                    //     "environment": "sandbox",
                    //     "product_name": "PayPal iOS SDK",
                    //     "paypal_sdk_version": "2.16.0",
                    //     "platform": "iOS"
                    //   },
                    //   "response_type": "payment",
                    //   "response": {
                    //     "id": "PAY-1AB23456CD789012EF34GHIJ",
                    //     "state": "approved",
                    //     "create_time": "2016-10-03T13:33:33Z",
                    //     "intent": "sale"
                    //   }
                    // }
                }, (e) => {
                    console.log(e);
                    // Error or render dialog closed without being successful
                });
            }, (e) => {
                console.log(e);
                // Error in configuration
            });
        }, (e) => {
            console.log(e);
            // Error in initialization, maybe PayPal isn't supported or something else
        });
    }

    patrocinadoresPage(){
        this.navCtrl.push(PatrocinadoresPage);
    }

    premiosPage(){
        this.navCtrl.push(PremiosPage);
    }

    preparacionPage(){
        this.navCtrl.push(PreparacionFisicaPage);
    }

    promocionesPage(){
        this.navCtrl.push(PromocionesPage);
    }
    encuestaPage(){
        this.navCtrl.push(EncuestaPage);
    }
    realidadPage(){
        this.browser.create("https://marcoredmg.github.io/avideo", '_system', {
            allowInlineMediaPlayback: 'yes'
        })
    }

    preRegistro(_carrera: Carrera){
        this.navCtrl.push(PreRegistroPage,{
            id: _carrera
        });
    }
    comunidadPage(_carrera: Carrera){
        this.navCtrl.push(ComunidadPage,{
            id: _carrera
        });
    }

    alertaSeguir(){
        let alerta = this.alertCtrl.create({
            title: "Sigues a este evento",
            subTitle: "Ahora sigues a este evento",
            buttons: ["OK"]
        });
        alerta.present();
    }

    seguirEvento(){
        let carreras_seguidas = (this.user.eventos_seguidos) ? this.user.eventos_seguidos:[];
        let seguidores = (this.carrera.seguidores) ? this.carrera.seguidores:[];
        carreras_seguidas.push(this.carrera);
        seguidores.push(this.user);
        this.database.doc("users/" + this.user.id).update({
            eventos_seguidos: carreras_seguidas
        });
        this.database.doc("carreras/" + this.carrera.id).update({
            seguidores: seguidores
        });
        this.carrera_seguidores = this.carrera_seguidores + 1;
        this.alertaSeguir();
    }

    dejarSeguirEvento(){

    }

    contains(a, obj) {
        for (var i = 0; i < a.length; i++) {
            if (a[i].nombre === obj) {
                return true;
            }
        }
        return false;
    }


}
