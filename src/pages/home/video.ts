import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';


@Component({
    selector: 'page-home',
    templateUrl: 'video.html'
})
export class VerVideoPage {

    constructor(
        public navCtrl: NavController, 
        public http: Http,
        public youtube: YoutubeVideoPlayer
    ) {

    }
    ionViewDidLoad(){
        
    }
}