import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Carrera } from '../../commons/carrera';
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal';

@Component({
  selector: 'page-inscripcion',
  templateUrl: 'inscripcion.html',
})
export class InscripcionPage {

  carrera: Carrera;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private payPal: PayPal,
  ) {
    this.carrera = this.navParams.get('id');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InscripcionPage');
  }

  payInscription(){
    console.log("LLamar paypal");
    this.payPal.init({
      PayPalEnvironmentProduction: 'AUmu_v627GtdTKLpxklC1Q-v3y0P3HN2EvQRj4e_ZcrTGCDk9XODhjEFf-9J_XkKaTjMsYFPm5jj9U5A',
      PayPalEnvironmentSandbox: 'AYnXw7j0ORCEKDAJlqCFMbMociws-Wb6kkFqIrCALZL7UuVK2oMpNe4DYOX9-gjvmrk8ShVwUfVDVuDA'
    }).then(() => {
      console.log("paypal init");
      // Environments: PayPalEnvironmentNoNetwork, PayPalEnvironmentSandbox, PayPalEnvironmentProduction
      this.payPal.prepareToRender('PayPalEnvironmentSandbox', new PayPalConfiguration({
        // Only needed if you get an "Internal Service Error" after PayPal login!
        //payPalShippingAddressOption: 2 // PayPalShippingAddressOptionPayPal
      })).then(() => {
        console.log("render");
        let payment = new PayPalPayment(this.carrera.costo, 'MXN', 'Pago de carrera: ' + this.carrera.nombre, 'sale');
        this.payPal.renderSinglePaymentUI(payment).then(() => {
          // Successfully paid
          
          // Example sandbox response
          //
          // {
          //   "client": {
          //     "environment": "sandbox",
          //     "product_name": "PayPal iOS SDK",
          //     "paypal_sdk_version": "2.16.0",
          //     "platform": "iOS"
          //   },
          //   "response_type": "payment",
          //   "response": {
          //     "id": "PAY-1AB23456CD789012EF34GHIJ",
          //     "state": "approved",
          //     "create_time": "2016-10-03T13:33:33Z",
          //     "intent": "sale"
          //   }
          // }
        }, (e) => {
          console.log(e);
          // Error or render dialog closed without being successful
        });
      }, (e) => {
        console.log(e);
        // Error in configuration
      });
    }, (e) => {
      console.log(e);
      // Error in initialization, maybe PayPal isn't supported or something else
    });
  }



}
