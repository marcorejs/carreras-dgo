import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Carrera } from '../../commons/carrera';
import { PhotoViewer } from '@ionic-native/photo-viewer';

@IonicPage()
@Component({
  selector: 'page-memes',
  templateUrl: 'memes.html',
})
export class MemesPage {

  carrera: Carrera;
  imagenes: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public photoViewer: PhotoViewer
  ) {
    this.carrera = this.navParams.get('id');
    this.imagenes = this.carrera.memes;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MemesPage');
  }

  viewMeme(img: string){
    console.log(img);
    this.photoViewer.show(img);
  }

}
