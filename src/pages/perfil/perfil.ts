import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { AngularFireAuth } from 'angularfire2/auth';
import { HomePage } from '../home/home';
import { User } from '../../commons/user';
import { UserConfigPage } from '../user-config/user-config';
import { Carrera } from '../../commons/carrera';
import { VerCarreraPage } from '../home/ver-carrera';


@IonicPage()
@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html',
})
export class PerfilPage {

  user: User;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    private afAuth: AngularFireAuth
  ) {
    this.user = this.navParams.get('id');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PerfilPage');
    console.log(this.user);
  }

  cerrarSesion(){
    let alerta = this.alertCtrl.create({
      title: 'Salir',
      message: "Estas seguro de salir?",
      buttons: [
        {
          text: 'Salir',
          handler: data => {
            this.afAuth.auth.signOut().then(() => {
              this.navCtrl.setRoot(HomePage);
            })
          }
        },
        {
          text: 'Cancelar',
          handler: data => {
            console.log('Cancelado');
          }
        }
      ]
    });

    alerta.present();
  }

  goToConfig(_user: User){
    this.navCtrl.push(UserConfigPage, {
      id: _user
    })
  }

  verCarrera(_carrera: Carrera) {
    this.navCtrl.push(VerCarreraPage, {
      id: _carrera,
      u: this.user
    });
  }

}
