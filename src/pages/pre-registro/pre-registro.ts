import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Carrera } from '../../commons/carrera';

/**
 * Generated class for the PreRegistroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-pre-registro',
  templateUrl: 'pre-registro.html',
})
export class PreRegistroPage {

  carrera: Carrera;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.carrera = this.navParams.get('id');
    console.log(this.carrera)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PreRegistroPage');
  }

}
