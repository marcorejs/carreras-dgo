import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RealidadPage } from './realidad';

@NgModule({
  declarations: [
    RealidadPage,
  ],
  imports: [
    IonicPageModule.forChild(RealidadPage),
  ],
})
export class RealidadPageModule {}
