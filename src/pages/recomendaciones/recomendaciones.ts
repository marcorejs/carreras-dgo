import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Carrera } from '../../commons/carrera';
import { User } from '../../commons/user';
import { AiProvider } from '../../providers/ai/ai';

@IonicPage()
@Component({
  selector: 'page-recomendaciones',
  templateUrl: 'recomendaciones.html',
})
export class RecomendacionesPage {

  carreras: Array<Carrera>;
  usuario: User;
  recomendaciones: Array<Carrera>;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private ai: AiProvider
  ) {
    this.carreras = this.navParams.get('id');
    this.usuario = this.navParams.get('u');
    this.recomendaciones = []
  }

  ionViewDidLoad() {
    this.probandoAI();
  }

  probandoAI() {
    this.carreras.forEach(carrera => {
      let compatibilidad = this.ai.teoremaAlternativoBayes(this.usuario, carrera, this.carreras);
      if (this.usuario.vmn <= compatibilidad) {
        this.recomendaciones.push(carrera);
      }
      console.log(compatibilidad);
    });

  }
}
