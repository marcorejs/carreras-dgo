import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Sesion } from '../../commons/sesion';
import { AngularFireAuth } from 'angularfire2/auth';
import { HomePage } from '../home/home';

import firebase from "firebase";

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  user = {} as Sesion;
  usuario: any = {nombre: "" };
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private afAuth: AngularFireAuth
  ) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  async registerUser(user: Sesion) {
    try {
      const result = await this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.password);
      if (result) {
        console.log(result);
        firebase.firestore().collection("users").add({
          email: user.email,
          nombre: this.usuario.nombre,
          vmn: 6
        })
        this.navCtrl.setRoot(HomePage);
      }
    }
    catch (e) {
      console.error(e);
    }
  }
}
