import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { User } from '../../commons/user';
import { AngularFirestore } from 'angularfire2/firestore';

@IonicPage()
@Component({
  selector: 'page-user-config',
  templateUrl: 'user-config.html',
})
export class UserConfigPage {

  user: User;
  deportes: Array<string> = ["Carrera pedestre", "Ciclismo", "Ciclismo de montaña", "Caminata"];
  deporte_fav: string;
  distancia: string;
  tipo_distancia: string;
  genero: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private database: AngularFirestore
  ) {
    this.user = this.navParams.get('id');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserConfigPage');
    this.deporte_fav = (this.user.deporte_fav) ? this.user.deporte_fav: undefined;
    this.distancia = (this.user.distancia_pref) ? this.user.distancia_pref: undefined;
    this.genero = (this.user.genero) ? this.user.genero: undefined;
    
  }

  saveConfig(){
    let preferencias = {
      deporte_fav: this.deporte_fav,
      distancia_pref: this.distanceCalculator(),
      genero: this.genero
    }
    this.database.doc("users/" + this.user.id).update(preferencias);
  }

  distanceCalculator(){
    let result = this.distancia;
    let auxiliar = parseInt(this.distancia);
    if (this.tipo_distancia == "millas"){
      auxiliar = auxiliar * 1.609;
      result = auxiliar.toString();
    }
    return result;
  }

}
