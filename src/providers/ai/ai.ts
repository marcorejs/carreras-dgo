import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../../commons/user';
import { Carrera } from '../../commons/carrera';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { disableDebugTools } from '@angular/platform-browser/src/browser/tools/tools';

@Injectable()
export class AiProvider {

  constructor(
    public alertCtrl: AlertController,
  ) {
    console.log('Hello AiProvider Provider');
  }

  costoPromedio(usuario: User) {
    let costo_promedio = 0;
    let contador_eventos = 0;
    if (usuario.eventos_seguidos) {
      usuario.eventos_seguidos.forEach(evento => {
        costo_promedio = costo_promedio + parseInt(evento.costo);
        contador_eventos++;
      });
      costo_promedio = costo_promedio / contador_eventos;
    }
    return costo_promedio;
  }

  recomendacionesDisponibles(usuario: User) {
    if (usuario.deporte_fav && usuario.distancia_pref && usuario.eventos_seguidos) {
      return true;
    } else {
      return false;
    }
  }

  alertaAIDisabled() {
    let alert = this.alertCtrl.create({
      title: "Servicio no disponible",
      subTitle: "Porfavor llena tus preferencias en la página de perfil y sigue a algunos eventos",
      buttons: ["OK"]
    });
    alert.present();
  }

  recomendarEventos(usuario: User, carrera: Carrera, carreras: Array<Carrera>) {
    if (this.recomendacionesDisponibles) {

    } else {
      this.alertaAIDisabled();
    }
  }

  teoremaAlternativoBayes(usuario: User, carrera: Carrera, carreras: Array<Carrera>) {
    if (this.recomendacionesDisponibles) {
      let resultado;
      const max = {
        deporte: 4,
        distancia: 2,
        costo: 1
      }
      let resultado_desc = {
        deporte: 0,
        costo: 0,
        distancia: 0
      }

      resultado_desc.deporte = this.eventosSeguidosDep(usuario, carreras, carrera, max.deporte);
      resultado_desc.distancia = this.eventosSeguidosDistance(usuario, carreras, carrera, max.distancia);
      resultado_desc.costo = this.eventosSeguidosCost(usuario, carreras, carrera, max.costo);

      resultado = resultado_desc.deporte + resultado_desc.costo + resultado_desc.distancia;
      return resultado;
    }
  }

  eventosSeguidosDep(usuario: User, carreras: Array<Carrera>, evento: Carrera, peso: number) {
    let result = 0;
    let disciplina = evento.disciplina;
    let ev_seguidos = 0;
    let num_carreras = 0;
    let pce = 0;
    let pc = 0;
    let pe = 0;

    usuario.eventos_seguidos.forEach(ev_seguido => {
      if (ev_seguido.disciplina == disciplina) {
        ev_seguidos++;
      }
    });

    carreras.forEach(carrera => {
      if (carrera.disciplina == disciplina) {
        num_carreras++;
      }
    })


    pce = ev_seguidos / num_carreras;
    pc = num_carreras / carreras.length;
    pe = usuario.eventos_seguidos.length / carreras.length;
    result = pce * pc;
    result = result / pe;
    result = result * peso;

    return result;
  }

  eventosSeguidosDistance(usuario: User, carreras: Array<Carrera>, evento: Carrera, peso: number) {
    let result = 0;
    let distancia = evento.distancia;
    let ev_seguidos = 0;
    let num_carreras = 0;
    let pce = 0;
    let pc = 0;
    let pe = 0;

    usuario.eventos_seguidos.forEach(ev_seguido => {
      if (ev_seguido.distancia == distancia) {
        ev_seguidos++;
      }
    });
    carreras.forEach(carrera => {
      if (carrera.distancia == distancia) {
        num_carreras++;
      }
    });

    pce = ev_seguidos / num_carreras;
    pc = num_carreras / carreras.length;
    pe = usuario.eventos_seguidos.length / carreras.length;

    result = pce * pc;
    result = result / pe;
    result = result * peso;

    return result;
  }

  eventosSeguidosCost(usuario: User, carreras: Array<Carrera>, evento: Carrera, peso: number) {
    let result = 0;
    let costo = evento.costo;
    let ev_seguidos = 0;
    let num_carreras = 0;
    let pce = 0;
    let pc = 0;
    let pe = 0;

    usuario.eventos_seguidos.forEach(ev_seguido => {
      if (ev_seguido.costo == costo) {
        ev_seguidos++;
      }
    });
    carreras.forEach(carrera => {
      if (carrera.costo == costo) {
        num_carreras++;
      }
    });

    pce = ev_seguidos / num_carreras;
    pc = num_carreras / carreras.length;
    pe = usuario.eventos_seguidos.length / carreras.length;

    result = pce * pc;
    result = result / pe;
    result = result * peso;

    return result;
  }

}
